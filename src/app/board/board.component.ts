import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Note } from '../notes/Note';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  closeResult: string;
  selectedColor:string='';
  selectedTitle: string='test';
  colors:string[]=[
    'lightgreen',
    'lightyellow',
    'lightblue',
    'magenta'
  ];

 
  notes: Note[]=(JSON.parse(localStorage["storedData"]));
  activenote: Note[];

  constructor(
    private modalService: NgbModal
    
    
  ) { }

  ngOnInit(): void {
    this.notes=(JSON.parse(localStorage["storedData"]));
  
   
 
  }
  
  radioChangeHandler(event:any){
    this.selectedColor=event.target.value;
   
  }
  titleChangeHandler(event:any){
    this.activenote[0].title=event.target.value;
   
  }
  textChangeHandler(event:any){
    this.activenote[0].noteText=event.target.value;
   
  }



  onSubmit(f: NgForm){
    this.notes.push(
        new Note(this.notes.length+1,f.value.title,f.value.noteText,this.selectedColor)
      );
   
  
    localStorage.setItem("storedData", JSON.stringify(this.notes));
    
  }

  onSave(f: NgForm){
    this.notes.map((currentItem) => {
      if (currentItem.id === this.activenote[0].id) {
        let i = this.notes.indexOf(currentItem)
       this.notes[i].title=this.activenote[0].title;
       this.notes[i].noteText=this.activenote[0].noteText;

      }
    });
  
    localStorage.setItem("storedData", JSON.stringify(this.notes));
    
  }

  buttonEdit(id: number, title: string, noteText: string, noteColor: string){
    this.activenote=[];
    this.activenote.push(
      new Note(id, title, noteText, noteColor)
      );
  }

  private removeNote(id:number): void {
    this.notes.map((currentItem) => {
      if (currentItem.id === id) {
        let i = this.notes.indexOf(currentItem)
        this.notes.splice(i, 1);

      }
    });
    for (const item of this.notes) {
      item.id = this.notes.indexOf(item) + 1
    }
    console.log(this.notes)
    localStorage.setItem("storedData", JSON.stringify(this.notes));
  }



  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
