export class Note {

    constructor(id: number, title: string, noteText: string, noteColor: string) {
      this.id=id
      this.title = title;
      this.noteText = noteText;
      this.noteColor=noteColor
    }
  
    id: number
    title: string;
    noteText: string;
    noteColor: string;
  }